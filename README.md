# SLMCMC

Stochastic-Likelihood MCMC (SLMCMC) is an efficient sampling method for population models (typically NLME models) given a population design objective. 
Naive sampling is very costly: it relies on a large number of individual samples to approximate the likelihood of a single population. 
Our method, on the other hand, uses only a handful of individual samples for each population. No approximation is made on the likelihood function.
The key concept of the method is that likelihoods of populations are never evaluated in the process.

We implement the SLMCMC method in R and apply it to a biological example.

## Version
The package was tested using R version 4.1.2 (2021-11-01).

## Installation

The following must be installed prior to installing the package:
* [R](https://www.r-project.org/)
* [Rtools](https://cran.r-project.org/bin/windows/Rtools/rtools40.html)
* `devtools` package. Can be installed by running `install.packages("devtools")` in R.
* [git](https://git-scm.com/)
* [pandoc](https://pandoc.org/installing.html) (only required to build the vignettes)
* `rmarkdown` package. Install by running `install.packages("rmarkdown`")` in R (only required to build the vignettes).

Then run the following command in R:
```R
devtools::install_git(url = "https://gitlab.com/csb.ethz/slmcmc", build_vignettes = TRUE)
```
Alternatively, you can set `build_vignettes = FALSE` if you do not wish to use the vignettes for documentation (Building the vignettes can take around a minute).

And attach the package with

```R
library(slmcmc)
```

## Dependencies

If there is a version problem with a dependency, you can try to install the versions of these packages we tested, for example using the `versions` packages.

```R
install.packages("versions")
versions::install.versions("stats", versions = "4.1.2")
versions::install.versions("fields", versions = "13.3")
versions::install.versions("coda", versions = "0.19-4")
versions::install.versions("fmcmc", versions = "0.5-1")
versions::install.versions("abind", versions = "1.4-5")
versions::install.versions("mvtnorm", versions = "1.1-3")
versions::install.versions("Matrix", versions = "1.4-0")
versions::install.versions("grDevices", versions = "4.1.2")
versions::install.versions("RColorBrewer", versions = "1.1-2")
```

Additionally, for online plotting during the sampling procedure with `PTMCMC()` we use the `x11()` function, which might not be installed on your system.
If so, and you do not wish to install it, set the argument `plot_progress = FALSE` in the `PTMCMC()` function to disable online plotting.


## Use
The package is articulated around the function `PTMCMC()`, which is a custom implementation of the Parallel Tempering algorithm on Metropolis-Hastings chains. Specifically, it can store in memory auxiliary variables that need to be used again in the tempered version of the SLMCMC algorithm. For details on the function and its parameters, see its documentation. To get quickly started, refer to the examples provided as vignettes, below.


## Examples

For two sampling examples, please see the vignettes:
```R
vignette("example_PTMCMC")                   # No populations involved
vignette("example_population_proba_PTMCMC")  # Sampling populations to achieve low variability
```


## Functions
The present package organizes functions as:
* Sampling functions: mainly `PTMCMC()` and its kernel functions `kernel_*`.
* Population tools: helper functions designed to help sample from populations, using `PTMCMC()`. Most notably, the function `logfPopProba()` can be helpful to design population objectives. `initDATA()` and `updateDATA()` serve to initialize a `DATA` object that will contain most of the necessary information to run the sampling.
* Convergence assessment: `getDiagnosticPTMCMC()`.
* Plotting: `plotDistrib()`, `plotTraceMCMC()` to plot resulting samples. `plotIndResp()` and `plotPopResp()` are useful plot functions to assess the sampling efficiency in the specific application where one wishes to design dose-response curves.

Type `?slmcmc` for a more comprehensive list of functions. See the functions' individual help files for more details.

## Author

* Baptiste Turpin <baptiste.turpin@bsse.ethz.ch>
