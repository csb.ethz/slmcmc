% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/PTMCMC.R
\name{geometric_kappa_ladder}
\alias{geometric_kappa_ladder}
\title{Geometric inverse temperature ladder}
\usage{
geometric_kappa_ladder(kappa_min, N, kappa_1_uniform = FALSE)
}
\arguments{
\item{kappa_min}{Minimum inverse temperature to use. Must be a number in ]0, 1[.}

\item{N}{Number of inverse temperatures in the ladder.}

\item{kappa_1_uniform}{Boolean, indicates whether the minimum inverse temperature
should be replaced with zero at the end of the computation. In terms of sampling, it means
the corresponding distribution will be the prior. Default to \code{FALSE}.}
}
\value{
Numeric vector of inverse temperatures, ordered from minimum to 1.
}
\description{
Compute a vector of inverse temperatures geometrically spaced,
starting at a specified value and ending at 1.
}
